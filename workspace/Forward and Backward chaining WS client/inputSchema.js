var inputSchema = {
	"type" : "object",
	"$schema" : "http://json-schema.org/draft-03/schema",
	"id" : "http://jsonschema.net",
	"required" : true,
	"properties" : {
		"clientQuery" : {
			"type" : "object",
			"id" : "http://jsonschema.net/process",
			"required" : true,
			"properties" : {
				"assertions" : {
					"type" : "array",
					"id" : "http://jsonschema.net/process/assertions",
					"required" : true,
					"items" : {
						"type" : "string",
						"id" : "http://jsonschema.net/process/assertions/0",
						"required" : true
					},
					"minItems": 1
				},
				"goal" : {
					"type" : "string",
					"id" : "http://jsonschema.net/process/goal",
					"required" : true
				},
				"rules" : {
					"type" : "array",
					"id" : "http://jsonschema.net/process/rules",
					"required" : true,
					"minItems" : 1,
					"items" : {
						"type" : "object",
						"id" : "http://jsonschema.net/process/rules/0",
						"required" : true,
						"properties" : {
							"antecedents" : {
								"type" : "array",
								"id" : "http://jsonschema.net/process/rules/0/antecedents",
								"required" : true,
								"minItems" : 1,
								"items" : {
									"type" : "string",
									"id" : "http://jsonschema.net/process/rules/0/antecedents/0",
									"required" : true
								}
							},
							"consequent" : {
								"type" : "string",
								"id" : "http://jsonschema.net/process/rules/0/consequent",
								"required" : true
							}
						}
					}
				},
				"mode" : {
					"type": "string",
					"enum": ["FORWARD", "BACKWARD"],
					"id" : "http://jsonschema.net/process/mode",
					"required" : true
				}
			}
		}
	}
};
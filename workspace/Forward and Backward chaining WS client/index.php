<?php
class WebService_ChainingServiceService_ChainingServicePort {
	var $_request_file;
	var $_soap_client;

	function WebService_ChainingServiceService_ChainingServicePort($path = './wsdl/ChainingServiceService.wsdl', $request_file = "request.json") {
		$this -> _soap_client = new SoapClient($path,  array(
   'cache_wsdl' => WSDL_CACHE_NONE,
   'trace' => true,
));
		$this -> request_file = $request_file;
	}

	function clientQuery($json_string) {
		// $json_string = file_get_contents($this -> request_file);
		$req = json_decode($json_string, true);
		$result = $this -> _soap_client -> __soapCall("clientQuery", array("parameters" => $req["clientQuery"]));
		return $result;
	}

};

$client = new WebService_ChainingServiceService_ChainingServicePort();
try {
	if (isset($_POST['code'])) {
		$result = $client -> clientQuery($_POST['code']);
	}
	/*
	?><pre><?php
		var_dump($result);
		?></pre><?php*/
	

} catch (Exception $e) {?><pre><?php
	var_dump($e);
	var_dump($client -> _soap_client -> __getLastRequest());
	?></pre><?php
}
// var_dump($_POST);
 ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
		<link rel="stylesheet" style="text/css" href="deps/opt/bootstrap.css" />
	</head>
	<body>
		<div class="body" style="display: block; width: 800px; margin: auto;">
			<div id="res" class="alert"></div>
			<form method="post"></form>
			<script type="text/javascript" src="deps/jquery.min.js"></script>
			<script type="text/javascript" src="deps/underscore.js"></script>
			<script type="text/javascript" src="deps/opt/jsv.js"></script>
			<script type="text/javascript" src="lib/jsonform.js"></script>
			<script type="text/javascript" src="deps/opt/ace/ace.js" ></script>
			<script type="text/javascript" src="deps/opt/ace/mode-json.js" ></script>
			<script type="text/javascript" src="deps/opt/jsonschema-b4.js" ></script>
			<script type="text/javascript" src="inputSchema.js" ></script>
			<script>
				function isValid() {
					try{
					var reqJSONString = jQuery.parseJSON($("#jsonform-0-elt-code__hidden").val());						
					}
					catch(err){
						console.log(err.message);
						$('#res').text(err.message);
						return false;
					}
					var validationResults = JSONSchema.validate(reqJSONString, inputSchema);
					console.log(validationResults);
					if(!validationResults.valid){
						$('#res').text("");
						$.each(validationResults.errors, function(index, value){
							$('#res').append("Property '"+value.property+"' "+ value.message+"<br />");
						});
					}
					return validationResults.valid;
				};

				$('form').jsonForm({
					"schema" : {
						"goalReached":{
							"type":"string",
							"title":"Goal reached"
						},
						"code" : {
							"type" : "string",
							"title" : "Query"
						},
						"executionSteps":{
							"type" :"string",
							"title":""
						},
						"ruleSequence":{
							"type" :"string",
							"title" :"Path"
							}
					},
					"form" : [{
						"key": "goalReached",
						"type": "<?php echo isset($result)?"text":"hidden"?>",
						"readonly":true,
						"value" : "<?php echo isset($result) ? $result->return->goalReached?"True":"False" : ""?>"
					},{
						"key" : "code",
						"type" : "ace",
						"aceMode" : "json",
						"aceTheme" : "twilight",
						"width" : "100%",
						"height" : "200px",
						"value": <?php echo isset($_POST['code']) ?  json_encode($_POST['code']):"\"\""; ?>
					},{
						"key": "executionSteps",
						"type": "<?php echo isset($result)?"textarea":"hidden"?>",
						"readonly":true,
						"value" : <?php echo isset($result) ?json_encode($result->return->executionSteps) :"\"\"" ?>
					},{
						"key": "ruleSequence",
						"type": "<?php echo isset($result)?"text":"hidden"?>",
						"readonly":true,
						"value" : "<?php $delim=""; $ret=""; 
							if(isset($result)){
							 $iteratedArr = is_array($result->return->ruleSequences->ruleSequence) ? $result->return->ruleSequences->ruleSequence : $result->return->ruleSequences;
							 foreach($iteratedArr as $rule){
								$ret .= $delim . $rule;
								$delim = ", ";
								};
							}
						echo $ret;
						?>"
					},{
						"type" : "button",
						"title" : "Execute",
						"onClick" : function(evt) {
								if (isValid()) {
									$('form').submit();
								} else {
									evt.preventDefault();
								}
							}
					}]
				});
			</script>
		</div>
	</body>
</html>

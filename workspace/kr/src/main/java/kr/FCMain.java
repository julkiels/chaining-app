package kr;

import java.util.LinkedList;

/**
 *
 * @author aurimas
 */
public class FCMain {

    public static void main(String[] args) {

        DataReader dr = new DataReader(args);

        LinkedList<Rule> rules = new LinkedList<Rule>();
        LinkedList<String> assertions = new LinkedList<String>();
        String goal = "";

        dr.readRules(rules);
        dr.readAssertions(assertions);
        goal = dr.readGoal();
        
        System.out.println("");
        System.out.println("1.Programos pradiniai duomenys");
        System.out.println("1.1.Taisykles:");
        for (Rule rule : rules) {System.out.println("  " + rule.toString());}

        System.out.println("1.2.Faktai:");
        System.out.print("  ");
        for (String assertion : assertions) {
            System.out.print(assertion);
            if (!assertion.equals(assertions.getLast()))
                    System.out.print(", ");
        }
        System.out.println("");
        
        System.out.println("1.3.Tikslas:");
        System.out.println("  " + goal);
        System.out.println("-----------------------------------------------");
        System.out.println("2.Programos vykdymas");
        ChainingProcessResult chainingProcessResult = new ForwardChaining(goal, rules, assertions, new StringBuilder()).execute();
        System.out.println(chainingProcessResult.getExecutionSteps());
        System.out.print(chainingProcessResult.getRuleSequenceString());
    }
}


package kr;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author aurimas
 */
public class DataReader {

    private final String dataRuleLine = "1) Taisykles";
    private final String dataAssertionsLine = "2) Faktai";
    private final String dataGoalLine = "3) Tikslas";
    
    private BufferedReader reader;

    public DataReader(String[] args) {
        String fileName = getFileName(args);
        try {
            reader = new BufferedReader(new FileReader(fileName));
        } catch (Exception ex) {
            System.out.println("Klaida skaitant faila." + "\n" + ex.getMessage());
        }
    }

    private String getFileName(String[] args) {
        String fileName;
        if (args.length < 1) {
            System.out.println("Iveskite duomenu failo pavadinima:");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                fileName = br.readLine();
                return fileName;
            } catch (Exception e) {
                System.out.println("Klaida nuskaitant duomenu faila");
            }
        } else {
            return args[0];
        }
        return null;
    }

    public void readAssertions(LinkedList<String> assertions) {
        String lineRead = "";
        try {
            while ((lineRead = reader.readLine()) != null) {
                lineRead = lineRead.trim();
                if (lineRead.isEmpty() || lineRead.startsWith("//")) {
                    continue;
                }
                if (lineRead.equalsIgnoreCase(dataAssertionsLine)) {
                    lineRead = reader.readLine();
                    String[] assertionsComments = lineRead.split("//");
                    String assertionsLine = assertionsComments[0].trim();
                    String[] assertionsRaw = assertionsLine.split("");
                    assertions.addAll(Arrays.asList(assertionsRaw));
                    assertions.poll();
                    return;
                }
            }
            throw new IllegalArgumentException("Nerastas "+dataAssertionsLine+" skyrius duomenu faile.");
        } catch (Exception ex) {
            System.out.println("Klaida skaitant faila." + "\n" + ex.getMessage());
        }
    }

    public void readRules(LinkedList<Rule> rules) {
        String lineRead = "";
        int ruleNumber = 0;
        try {
            while ((lineRead = reader.readLine()) != null) {
                if (lineRead.isEmpty()) {
                    continue;
                }
                if (lineRead.equalsIgnoreCase(dataRuleLine)) {
                    while ((lineRead = reader.readLine()) != null && !lineRead.isEmpty()) {
                        String[] raw = lineRead.split("");
                        int i = 2;
                        LinkedList<String> antecedents = new LinkedList<String>();
                        while(!raw[i].equals(" ") && !raw[i].equals("/"))
                            antecedents.add(raw[i++]);                       
                        ruleNumber++;
                        Rule rule = new Rule(raw[1], antecedents, ruleNumber, false);
                        rules.add(rule);
                    }
                    return;
                }
            }
            throw new IllegalArgumentException("Nerastas "+ dataRuleLine+" skyrius duomenu faile.");
        } catch (Exception ex) {
            System.out.println("Klaida skaitant faila." + "\n" + ex.getMessage());
        }
    }

    public String readGoal() {
        String lineRead = "";
        try {
            while ((lineRead = reader.readLine()) != null) {
                if (lineRead.isEmpty()) {
                    continue;
                }
                if (lineRead.equalsIgnoreCase(dataGoalLine)) {
                    lineRead = reader.readLine();
                    if (lineRead == null || lineRead.isEmpty()) {
                        throw new IllegalArgumentException("Nerastas vykdymo tikslas. Patikrinkite"
                                + " duomenu failo "+dataGoalLine+" skyriu");
                    }                   
                    return lineRead;
                }
            }
            throw new IllegalArgumentException("Nerastas 'Tikslas:' skyrius duomenu faile.");
        } catch (Exception ex) {
            System.out.println("Klaida skaitant faila." + "\n" + ex.getMessage());
        }
        return null;
    }
}

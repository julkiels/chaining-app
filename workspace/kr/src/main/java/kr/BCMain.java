package kr;

import java.util.LinkedList;

/**
 * 
 * @author aurimas
 */
public class BCMain {
	public static String NL = System.getProperty("line.separator");

	public static void main(String[] args) {
		DataReader dr = new DataReader(args);
		StringBuilder stringBuilder = new StringBuilder();

		LinkedList<Rule> rules = new LinkedList<Rule>();
		LinkedList<String> assertions = new LinkedList<String>();
		String goal = "";

		dr.readRules(rules);
		dr.readAssertions(assertions);
		goal = dr.readGoal();
		stringBuilder.append(NL).append("1.Programos pradiniai duomenys")
				.append(NL).append("1.1.Taisykles:").append(NL);
		for (Rule rule : rules) {
			stringBuilder.append("  " + rule.toString()).append(NL);
		}

		stringBuilder.append("1.2.Faktai:").append(NL).append("  ");
		for (String assertion : assertions) {
			stringBuilder.append(assertion);
			if (!assertion.equals(assertions.getLast())) {
				stringBuilder.append(", ");
			}
		}
		stringBuilder.append(NL).append("1.3.Tikslas:").append(NL)
				.append("  " + goal).append(NL)
					.append("-----------------------------------------------").append(NL)
		.append("2.Programos vykdymas").append(NL)
		.append("2.1.Vykdymo protokolas:\n").append(NL);

		BackwardChaining bc = new BackwardChaining(goal, rules, assertions, stringBuilder);
		ChainingProcessResult chainingResult = bc.execute();
		String successString = chainingResult.getGoalReached() ? "  Tikslas PASIEKTAS."
				: "  Tikslas NEPASIEKTAS.";
		stringBuilder.append(NL).append("3.Atsakymas:").append(NL);
		stringBuilder.append(successString).append(NL);
		String result = "";
		if (chainingResult.getGoalReached() && ((result = chainingResult.getRuleSequenceString()) != null)) {
			stringBuilder.append(result).append(NL);
		} else {
			if (chainingResult.getGoalReached())
				stringBuilder.append("  Tikslas buvo pradineje faktu aibeje.").append(NL);
		}
		System.out.print(stringBuilder.toString());
		
	}
}

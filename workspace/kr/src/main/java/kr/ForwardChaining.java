package kr;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author aurimas
 */
public class ForwardChaining extends AbstractChaining{

	String step;
    private int lineCount;

    public ForwardChaining(String goal, List<Rule> rules, List<String> assertions, StringBuilder sb) {
        super(goal , rules, assertions, sb);
        lineCount = 0;
    }

    protected ChainingProcessResult doExecute() {
        List<Rule> ruleList = new LinkedList<Rule>();
        int executionNumber = 0;
        getSb().append("PART 2").append(NL).append(NL);
        getSb().append("Trace:").append(NL);
        boolean assertions_changed;
        if (getAssertions().contains(getGoal())) {
            return new ChainingProcessResult(getSb(), true, ruleList);
        } else {
        }
        do {
            executionNumber++;
            getSb().append(getLineCount() + executionNumber + " iteration").append(NL);
            assertions_changed = false;
            for (Rule rule : getRules()) {
                getSb().append(getLineCount()+ " "+ rule.toString());
                if (!rule.getisUsed()) {
                    if (getAssertions().containsAll(rule.getAntecedents())) {
                        rule.setisUsed(Boolean.TRUE);
                        if (!getAssertions().contains(rule.getConsequent())) {
                            ruleList.add(rule);
                            getAssertions().add(rule.getConsequent());
                            getSb().append(" raise flag1. Facts: ");
							String delim = "";
                            for (String s : getAssertions()) {
								getSb().append(delim).append(s);
								delim =", ";
                            }
                            getSb().append(".").append(NL);
                            assertions_changed = true;
                            if (rule.getConsequent().equals(getGoal())) {
                                return new ChainingProcessResult(getSb(), true, ruleList);
                            } 
                        }
                    } else {
                        getSb().append(" skipped, because of a missing fact ");
                        for(String antecedent: rule.getAntecedents()){
                        	if (!getAssertions().contains(antecedent)) {
								getSb().append(antecedent);
								break;
							}
                        }
                        getSb().append(".").append(NL);
                        continue;
                    }
                } else {
                    getSb().append(" skipped, because flag1 raised.").append(NL);
                    continue;
                }
                if (assertions_changed) {
                    break;
                } else {
                	getSb().append(" skipped, because the consequent is a fact, raise flag2.").append(NL);
                }
            }
            getSb().append(NL);
        } while (assertions_changed);
        return new ChainingProcessResult(getSb(), true, ruleList);
    }

//    public ChainingProcessResult formatRuleList(boolean success, List<Integer> ruleList) {
//    	List<String> ruleSequence = new ArrayList<String>();
//    	for(Integer ruleNr: ruleList){
//    		ruleSequence.add(String.format("R%d", ruleNr));
//    	}  
//		return new ChainingProcessResult(getSb().toString(), success, ruleSequence);
//    }
    
    public String getLineCount() {
        StringBuilder sb = new StringBuilder();
        String lineNumber = Integer.toString(++lineCount);
        sb.append(lineNumber);
        for (int i = 0; i < 3-lineNumber.length(); i++) {
        	sb.append(" ");          
        }
        sb.append("   ");
        return sb.toString();
    }
}

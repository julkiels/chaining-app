package kr;
import java.util.ArrayList;
import java.util.List;

public class Rule {
    
    private String consequent;
    private List<String> antecedents;
    private int number;
    private Boolean isUsed;

    public Rule(String consequent, List<String> antecedents, int number, Boolean used) {
        this.consequent = consequent;
        this.antecedents = new ArrayList<String>(antecedents);
        this.number = number;
        this.isUsed = used;
    }

    public Boolean getisUsed() {
        return isUsed;
    }

    public void setisUsed(Boolean used) {
        this.isUsed = used;
    }

    public int getNumber() {
        return number;
    }

    public List<String> getAntecedents() {
        return antecedents;
    }

    public String getConsequent() {
        return consequent;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("R").append(Integer.toString(number)).append(": ");
        for(String anc : antecedents) {
            sb.append(anc).append(", ");
        }
        sb.delete(sb.lastIndexOf(", "), sb.length());
        sb.append(" -> ").append(consequent);
        return sb.toString();
    }
}

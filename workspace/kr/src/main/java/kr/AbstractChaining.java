package kr;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractChaining {

	protected static String NL = System.getProperty("line.separator");

	private String goal;
	private List<Rule> rules;
	private List<String> assertions;
	private StringBuilder sb;


	public AbstractChaining(String goal, List<Rule> rules,
			List<String> assertions, StringBuilder sb) {
		super();
		this.goal = goal;
		this.rules = new ArrayList<Rule>(rules);
		this.assertions = new ArrayList<String>(assertions);
		this.sb = sb;
	}

	public String getGoal() {
		return goal;
	}

	public List<Rule> getRules() {
		return rules;
	}

	public List<String> getAssertions() {
		return assertions;
	}

	public StringBuilder getSb() {
		return sb;
	}

	public ChainingProcessResult execute() {
		this.sb.append("PART 1").append(NL).append(NL)
			.append("1.1. Rules: ").append(NL);
		for(Rule r: rules){
			this.sb.append("  ").append(r.toString()).append(NL);
		}
		this.sb.append(NL);
		this.sb
		.append("1.2. Facts: ").append(assertions.toString()).append(NL).append(NL)
		.append("1.3. Goal: ").append(goal).append(NL).append(NL);
		return doExecute();
	};
	
	protected abstract ChainingProcessResult doExecute();
}

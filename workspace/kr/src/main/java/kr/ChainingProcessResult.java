package kr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChainingProcessResult {
	
	protected static String NL = System.getProperty("line.separator");
	private final String executionSteps;
	private final Boolean goalReached;
	private List<String> ruleSequence;
	private String ruleSequenceString;

	public ChainingProcessResult(StringBuilder sb, Boolean goalReached, List<Rule> ruleSeq){
		super();
		this.goalReached = goalReached;
		this.ruleSequence = formatRuleList(ruleSeq);
		this.executionSteps = formatAnswer(sb, goalReached, ruleSequence); 
	};
	
	private String formatAnswer(StringBuilder sb, Boolean goalReached,
			List<String> ruleSequence) {
		sb.append(NL);
		sb.append("3. RESULTS").append(NL);
		sb.append(" Goal").append(goalReached ? " reached":" failed").append(".").append(NL);
		if(goalReached){
			sb.append(" Path: ").append(getRuleSequenceString()).append(".");
		}
		return sb.toString();
	}

	private  List<String> formatRuleList( List<Rule> ruleList) {
    	List<String> ruleSequence = new ArrayList<String>();
    	for(Rule rule: ruleList){
    		ruleSequence.add(String.format("R%d", rule.getNumber()));
    	}  
    	return Collections.unmodifiableList(ruleSequence);
    }
	
	public String getExecutionSteps() {
		return executionSteps;
	}

	public Boolean getGoalReached() {
		return goalReached;
	}

	public List<String> getRuleSequence() {
		return ruleSequence;
	}
	public String getRuleSequenceString(){
		if (ruleSequenceString == null) {
			StringBuilder stringBuilder = new StringBuilder();
			String delim = "";
			for (String item : ruleSequence) {
				stringBuilder.append(delim).append(item);
				delim = ", ";
			}
			if(ruleSequence.size() == 0 ){
				stringBuilder.append("empty");
			}
			ruleSequenceString = stringBuilder.toString();
		}
		return ruleSequenceString;
	}
}

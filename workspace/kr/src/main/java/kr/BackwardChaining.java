package kr;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aurimas
 * @author julius
 */
public class BackwardChaining extends AbstractChaining{
	
    private List<String> goals;
    private List<Rule> resultPath;
    private int recursionLevel;
    private int lineCount;

    public BackwardChaining(String goal, List<Rule> rules, List<String> assertions, StringBuilder sb) {
    	super(goal, rules, assertions, sb);
        resultPath = new ArrayList<Rule>();
        goals = new ArrayList<String>();
        lineCount = 0;
    }
    
    protected ChainingProcessResult doExecute(){
    	getSb().append("PART 2").append(NL).append(NL);
    	getSb().append("Trace:").append(NL);
    	boolean success = backwardChaining(getGoal());
		return new ChainingProcessResult(getSb(), success, resultPath);
    }

    private boolean backwardChaining(String currentGoal) {
        recursionLevel++;
        if (getAssertions().contains(currentGoal)) {
        	getSb().append(getRecursionLevel()).append("Goal ").append(currentGoal).append(".");
            getSb().append(" Fact.").append(NL);
            recursionLevel--;
            return true;
        }
        goals.add(currentGoal);
        for (Rule rule : getRules()) {
            if (!rule.getisUsed() && rule.getConsequent().equals(currentGoal)) {
                List<String> antecedents = rule.getAntecedents();
                boolean antecendentsExists = true;
            	getSb().append(getRecursionLevel()).append("Goal ").append(currentGoal).append(".")
        			.append(" Take ").append(rule.toString()).append(".")
        				.append(" New goals ").append(antecedents.toString()).append(".").append(NL);
                for (String antecedent : antecedents) {
                    if (!goals.contains(antecedent)) {
                        if (!backwardChaining(antecedent)) {
                            antecendentsExists = false;
                            break;
                        }
                    } else {
                    	recursionLevel++;
                        getSb().append(getRecursionLevel()).append("Goal ").append(antecedent).append(". Loop.").append(NL);
                    	recursionLevel--;
                        antecendentsExists = false;
                    }
                }
                if (antecendentsExists) {
                    rule.setisUsed(true);
                    getAssertions().add(rule.getConsequent());
                    resultPath.add(rule);
                    getSb().append(getRecursionLevel()).append("Goal ").append(currentGoal)
                    	.append(". New fact: ").append(rule.getConsequent())
                    	.append(". Facts: ").append(getAssertions()).append(".").append(NL);
                    recursionLevel--;
                    goals.remove(currentGoal);
                    return true;
                }
            }
        }
        goals.remove(currentGoal);
        for(Rule rule: getRules()){
        	if(rule.getisUsed()){
        		rule.setisUsed(false);
        		resultPath.remove(rule);
        		getAssertions().remove(rule.getConsequent());
        	}
        }
    	getSb().append(getRecursionLevel()).append("Goal ").append(currentGoal).append(".");
        getSb().append(" Fact cannot be derived").append(".").append(NL);
        recursionLevel--;
        return false;
    }

	public String getResultPath() {
        if (resultPath.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        getSb().append("  Path: {");
        for (Rule rule : resultPath) {
        	sb.append("R").append(Integer.toString(rule.getNumber())).append(", ");
        }
        if (sb.lastIndexOf(", ") != -1) {
        	sb.delete(getSb().lastIndexOf(", "), getSb().length());
        }
        sb.append("}");
        return sb.toString();
    }

    public String getRecursionLevel() {
        StringBuilder level = new StringBuilder();
        String lineNumber = Integer.toString(++lineCount);
        level.append(lineNumber);
        
        for (int i = 0; i < 3 - lineNumber.length(); i++) {
            level.append(" ");
        }
        
        for (int i = 0; i < recursionLevel; i++) {
            level.append("   ");
        }
        
        return level.toString();
    }
}

package kr;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

public class ForwardChainingTestCase {

	@Test
	public void test() {
		String goal = "Z";
		List<Rule> rules = new ArrayList<Rule>();
		rules.add(new Rule("B", Arrays.asList("A"), 1, false));
		rules.add(new Rule("E", Arrays.asList("A"), 2, false));
		rules.add(new Rule("C", Arrays.asList("B"), 3, false));
		rules.add(new Rule("Z", Arrays.asList("C","D"), 4, false));
		List<String> assertions = new ArrayList<String>();
		assertions.add("A");
		assertions.add("E");
		assertions.add("D");
		StringBuilder sb = new StringBuilder();
		ForwardChaining forwardChaining = new ForwardChaining(goal, rules, assertions, sb);
		ChainingProcessResult result = forwardChaining.execute();
		System.out.print(sb.toString());
		TestCase.assertTrue(result.getGoalReached());
	}

}

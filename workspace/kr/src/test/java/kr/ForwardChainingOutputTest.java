package kr;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class ForwardChainingOutputTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		StringBuilder sb = new StringBuilder();
		List<Rule> rules = new ArrayList<Rule>();
		rules.add(new Rule("C",
				Arrays.asList(new String[]{"B"}),
				1,
				false ));
		rules.add(new Rule("B",
						Arrays.asList(new String[]{"A"}),
							2,
							false ));
		rules.add(
				new Rule("Z",
						Arrays.asList(new String[]{"D","C"}),
						3,
						false ));
		List<String> assertions = new ArrayList<String>();
		assertions.add("A");
		assertions.add("D");
		ForwardChaining chaining = new ForwardChaining("Z", rules, assertions, sb);
		ChainingProcessResult result = chaining.execute();
		System.out.println(sb.toString());
		assertTrue(result.getGoalReached());
	}

}

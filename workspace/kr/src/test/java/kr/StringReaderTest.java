package kr;

import static org.junit.Assert.fail;

import java.util.StringTokenizer;

import org.junit.Test;

public class StringReaderTest {
	private static String NL = System.getProperty("line.separator");

	@Test
	public void test() {
		StringBuilder builder = new StringBuilder();
		builder.append("a1").append(NL);
		builder.append("a2").append(NL);
		builder.append("a3").append(NL);
		StringTokenizer stringTokenizer = new StringTokenizer(builder.toString());
		while(stringTokenizer.hasMoreElements()){
			String token = stringTokenizer.nextToken();
			System.out.format("Token %s" + NL, token);
		}
//		fail("Not yet implemented");
	}

}

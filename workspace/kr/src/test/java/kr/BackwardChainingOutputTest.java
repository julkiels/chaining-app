package kr;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class BackwardChainingOutputTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test1() {
		StringBuilder sb = new StringBuilder();
		List<Rule> rules = new ArrayList<Rule>();
		rules.add(new Rule("Z", asList(new String[]{"D","C"}), 1, false ));
		rules.add(new Rule("D", asList(new String[]{"C"}), 2, false ));
		rules.add(new Rule("C", asList(new String[]{"B"}), 3, false ));
		rules.add(new Rule("B", asList(new String[]{"A"}), 4, false ));
		rules.add(new Rule("A", asList(new String[]{"D"}), 5, false ));
		rules.add(new Rule("D", asList(new String[]{"T"}), 6, false ));
		rules.add(new Rule("A", asList(new String[]{"G"}), 7, false ));
		rules.add(new Rule("B", asList(new String[]{"H"}), 8, false ));
		rules.add(new Rule("C", asList(new String[]{"J"}), 9, false ));
		List<String> assertions = new ArrayList<String>();
		assertions.add("T");
		BackwardChaining bc = new BackwardChaining("Z", 
				rules, 
				assertions, sb);
		ChainingProcessResult result = bc.execute();
		System.out.println(sb.toString());
		TestCase.assertTrue(result.getGoalReached());
	}
	
	@Test
	public void test2() {
		StringBuilder sb = new StringBuilder();
		List<Rule> rules = new ArrayList<Rule>();
		rules.add(new Rule("Z", asList(new String[]{"C","D"}), 1, false ));
		rules.add(new Rule("C", asList(new String[]{"T"}), 2, false ));
		rules.add(new Rule("Z", asList(new String[]{"T"}), 3, false ));
		List<String> assertions = new ArrayList<String>();
		assertions.add("T");
		BackwardChaining bc = new BackwardChaining("Z", 
				rules, 
				assertions, sb);
		ChainingProcessResult result = bc.execute();
		System.out.println(sb.toString());
		TestCase.assertTrue(result.getGoalReached());
	}
	
//	@Test
//	public void test3() {
//		StringBuilder sb = new StringBuilder();
//		List<Rule> rules = new ArrayList<Rule>();
//		rules.add(new Rule("Z", asList(new String[]{"A"}), 1, false ));
//		rules.add(new Rule("A", asList(new String[]{"C"}), 2, false ));
//		rules.add(new Rule("Z", asList(new String[]{"T"}), 3, false ));
//		List<String> assertions = new ArrayList<String>();
//		assertions.addAll(Arrays.asList("T","C"));
//		BackwardChaining bc = new BackwardChaining("Z", 
//				rules, 
//				assertions, sb);
//		ChainingProcessResult result = bc.execute();
//		System.out.println(sb.toString());
//		TestCase.assertTrue(result.getGoalReached());
//	}

}

package lt.vu.mif.chaining.client;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ChainingResp implements Serializable,IsSerializable{
	
	private static final long serialVersionUID = 6677457848394761328L;
	private String execution;
	private Boolean goalReached;
	private String ruleseq;
	
	public ChainingResp(){};
	
	public ChainingResp(String execution, Boolean goalReached, String ruleseq) {
		super();
		this.execution = execution;
		this.goalReached = goalReached;
		this.ruleseq = ruleseq;
	}

	public String getExecution() {
		return execution;
	}

	public void setExecution(String execution) {
		this.execution = execution;
	}

	public Boolean getGoalReached() {
		return goalReached;
	}

	public void setGoalReached(Boolean goalReached) {
		this.goalReached = goalReached;
	}

	public String getRuleseq() {
		return ruleseq;
	}

	public void setRuleseq(String ruleseq) {
		this.ruleseq = ruleseq;
	}
	
}

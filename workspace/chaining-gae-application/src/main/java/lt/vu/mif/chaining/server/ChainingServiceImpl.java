package lt.vu.mif.chaining.server;

import java.util.LinkedList;

import javax.jws.WebMethod;
import javax.jws.WebService;

import kr.BackwardChaining;
import kr.ChainingProcessResult;
import kr.ForwardChaining;
import kr.Rule;
import lt.vu.mif.chaining.client.ChainingResp;
import lt.vu.mif.chaining.client.ChainingService;
import lt.vu.mif.chaining.util.ChainingUtils;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@WebService
public class ChainingServiceImpl extends RemoteServiceServlet implements
		ChainingService {

	public static final String NL = System.getProperty("line.separator");

	private static final long serialVersionUID = -2289727655609089279L;

	@Override
	@WebMethod
	public ChainingResp forwardChaining(String ruleStr, String assertionsStr,
			String goalString) {
		LinkedList<Rule> rulesList = new LinkedList<Rule>();
		ChainingUtils.readRules(ruleStr, rulesList);

		LinkedList<String> assertionsList = new LinkedList<String>();
		ChainingUtils.readAssertion(assertionsStr, assertionsList);

		StringBuilder sb = new StringBuilder();
		ForwardChaining forwardChaining = new ForwardChaining(
				ChainingUtils.readGoal(goalString), rulesList, assertionsList,
				sb);
		ChainingProcessResult processingResult = forwardChaining.execute();
		formatResult(sb, processingResult);
		return new ChainingResp(processingResult.getExecutionSteps(), processingResult.getGoalReached(), processingResult.getRuleSequenceString());
	}

	@Override
	@WebMethod
	public ChainingResp backwardChaining(String ruleStr, String assertionsStr,
			String goalString) {
		LinkedList<Rule> rulesList = new LinkedList<Rule>();
		ChainingUtils.readRules(ruleStr, rulesList);

		LinkedList<String> assertionsList = new LinkedList<String>();
		ChainingUtils.readAssertion(assertionsStr, assertionsList);

		StringBuilder sb = new StringBuilder();
		BackwardChaining backwardChaining = new BackwardChaining(goalString,
				rulesList, assertionsList, sb);
		ChainingProcessResult processingResult = backwardChaining.execute();
		sb.append(processingResult.getExecutionSteps());
		formatResult(sb, processingResult);
		return new ChainingResp(processingResult.getExecutionSteps(), processingResult.getGoalReached(), processingResult.getRuleSequenceString());
	}

	private void formatResult(StringBuilder sb,
			ChainingProcessResult processingResult) {
		sb.append(String.format("Tikslas %spasiektas\n", processingResult.getGoalReached() ? ""
				: "ne"));
		sb.append(String.format("Planas: %s", processingResult.getRuleSequenceString()));
	}

	@Override
	public String toJson(String mode, String ruleStr, String assertionsStr,
			String goalString) {
		LinkedList<Rule> rulesList = new LinkedList<Rule>();
		ChainingUtils.readRules(ruleStr, rulesList);

		LinkedList<String> assertionsList = new LinkedList<String>();
		ChainingUtils.readAssertion(assertionsStr, assertionsList);
		JSONObject rootObj = null;
		try {
			rootObj = new JSONObject();
			JSONObject queryObj = new JSONObject();
			queryObj.put("mode",mode.toUpperCase());
			queryObj.put("goal", goalString);
			queryObj.put("assertions", new JSONArray(assertionsList));
			JSONArray rulesArr = new JSONArray();
			for(Rule rule: rulesList){
				JSONObject ruleObj = new JSONObject();
				ruleObj.put("antecedents",rule.getAntecedents());
				ruleObj.put("consequent", rule.getConsequent());
				rulesArr.put(ruleObj);
			}
			queryObj.put("rules",rulesArr);
			rootObj.put("clientQuery", queryObj);
			return rootObj.toString(3);			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
}

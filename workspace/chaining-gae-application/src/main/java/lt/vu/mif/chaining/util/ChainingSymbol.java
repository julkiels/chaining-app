package lt.vu.mif.chaining.util;

public class ChainingSymbol {

	private Character ruleChar;
	private Byte ruleByte;

	public ChainingSymbol(String chainingSymbolStr) {
		this(chainingSymbolStr.charAt(0), chainingSymbolStr.length() > 1 ? Byte
				.parseByte(chainingSymbolStr.substring(1)) : null);
	}

	private ChainingSymbol(Character ruleChar, Byte ruleByte) {
		super();
		if (!Character.isLetter(ruleChar)) {
			throw new IllegalArgumentException(String.format(
					"ruleChar '%c' must be letter", ruleChar));
		}
		if (ruleByte != null && Math.signum(ruleByte.doubleValue()) < 0) {
			throw new IllegalArgumentException(String.format(
					"ruleByte '%d' must be positive value", ruleByte));
		}
		this.ruleChar = Character.toUpperCase(ruleChar);
		this.ruleByte = ruleByte;
	}

	public Character getRuleChar() {
		return ruleChar;
	}

	public Byte getRuleByte() {
		return ruleByte;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ruleByte == null) ? 0 : ruleByte.hashCode());
		result = prime * result
				+ ((ruleChar == null) ? 0 : ruleChar.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChainingSymbol other = (ChainingSymbol) obj;
		if (ruleByte == null) {
			if (other.ruleByte != null)
				return false;
		} else if (!ruleByte.equals(other.ruleByte))
			return false;
		if (ruleChar == null) {
			if (other.ruleChar != null)
				return false;
		} else if (!ruleChar.equals(other.ruleChar))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(ruleChar.toString());
		if (ruleByte != null) {
			sb.append(ruleByte.toString());
		}
		return sb.toString();
	}

}

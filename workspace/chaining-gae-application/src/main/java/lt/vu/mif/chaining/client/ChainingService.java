package lt.vu.mif.chaining.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("chaining")
public interface ChainingService extends RemoteService {

	ChainingResp forwardChaining(String rules, String assertions, String goal);

	ChainingResp backwardChaining(String rules, String assertions, String goal);
	
	String toJson(String mode, String rules, String assertions, String goal);

}

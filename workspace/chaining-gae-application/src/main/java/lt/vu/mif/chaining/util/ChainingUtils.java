package lt.vu.mif.chaining.util;

import java.util.List;
import java.util.StringTokenizer;

import kr.Rule;
import lt.vu.mif.chaining.server.ChainingServiceImpl;

public class ChainingUtils {

	public static void readRules(String rulesStr, List<Rule> ruleList){
		StringTokenizer stringTokenizer = new StringTokenizer(rulesStr, ChainingServiceImpl.NL );
		String lineRead = null;
		int ruleNumber = 0;
		List<ChainingSymbol> parsedSymbols = null;
		while(stringTokenizer.hasMoreTokens()){
			lineRead = stringTokenizer.nextToken();
			parsedSymbols = ChainingSymbolsParser.parse(lineRead);
	        ruleNumber++;
	        Rule rule = new ChainingRule(parsedSymbols.remove(0), parsedSymbols, ruleNumber, false);
	        ruleList.add(rule);
		}
	}

	public static void readAssertion(String assertionsStr, List<String> assertionsList){
		StringTokenizer stringTokenizer = new StringTokenizer(assertionsStr);
		String lineRead = null;
		List<ChainingSymbol> parsedList = null;
		while (stringTokenizer.hasMoreTokens()) {
			lineRead = stringTokenizer.nextToken();
			parsedList = ChainingSymbolsParser.parse(lineRead);
	        for(ChainingSymbol cb: parsedList){
	        	assertionsList.add(cb.toString());
	        };
	    }
	}

	public  static String readGoal(String goalStr){
		return new ChainingSymbol(goalStr).toString();
	}

}

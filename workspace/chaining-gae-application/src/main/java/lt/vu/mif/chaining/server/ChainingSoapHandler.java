package lt.vu.mif.chaining.server;

import java.util.Iterator;

import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SAAJResult;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.dom.DOMSource;

import lt.vu.mif.chaining.server.ws.ChainingAdapter;

public class ChainingSoapHandler {

	private ChainingAdapter chainingAdapter;
	private MessageFactory messageFactory;

	public ChainingSoapHandler() throws SOAPException{
		messageFactory = MessageFactory.newInstance();
		chainingAdapter = new ChainingAdapter();
	}

	@SuppressWarnings("rawtypes")
	public SOAPMessage handleSOAPRequest(SOAPMessage request) throws SOAPException{
		SOAPBody soapBody = request.getSOAPBody();
		Iterator iterator = soapBody.getChildElements();
		Object responsePojo = null;
		while (iterator.hasNext()) {
			Object next = iterator.next();
			if (next instanceof SOAPElement) {
				SOAPElement soapElement = (SOAPElement) next;
				QName qname = soapElement.getElementQName();
				if(lt.vu.mif.chaining.gen.ObjectFactory._ClientQuery_QNAME.equals(qname)){
					lt.vu.mif.chaining.jaxws.ClientQuery unmarshaledReq = JAXB.unmarshal(new DOMSource(soapElement), lt.vu.mif.chaining.jaxws.ClientQuery.class);					
					responsePojo = chainingAdapter.chaining(unmarshaledReq);
				}
			}
		}

		SOAPMessage soapResponse = messageFactory.createMessage();
		soapBody = soapResponse.getSOAPBody();
		if (responsePojo != null) {
			JAXB.marshal(responsePojo, new SAAJResult(soapBody));
		} else {
			SOAPFault fault = soapBody.addFault();
			fault.setFaultString("Unrecognized SOAP request.");
		}
		return soapResponse;
	}
}

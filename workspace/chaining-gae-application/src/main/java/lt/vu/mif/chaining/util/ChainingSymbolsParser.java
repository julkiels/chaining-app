package lt.vu.mif.chaining.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChainingSymbolsParser {
	private static Pattern characterPatter =  Pattern.compile("([a-zA-Z][0-9]*)");

	public static List<ChainingSymbol> parse(String row){
		ArrayList<ChainingSymbol> symbols = new ArrayList<ChainingSymbol>();
		String trimedRow = row.trim();
		int indexOfCommentsStart = trimedRow.indexOf("//");
		if(indexOfCommentsStart >= 0){
			trimedRow = trimedRow.substring(0,trimedRow.indexOf("//"));			
		}
		String replacedSymbolsRow = trimedRow.replaceAll("/^[a-zA-Z0-9]/", "");
		if(replacedSymbolsRow.length() == 0 ) {
			throw new IllegalArgumentException("trimmed row length must above 0");
		}
		Matcher matcher = characterPatter.matcher(replacedSymbolsRow);
		while (matcher.find()){
			String match = matcher.group();
			symbols.add(new ChainingSymbol(match));
		}
		return symbols;
	}
}

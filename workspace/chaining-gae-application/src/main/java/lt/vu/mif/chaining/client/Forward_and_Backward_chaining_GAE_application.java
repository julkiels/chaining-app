package lt.vu.mif.chaining.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Forward_and_Backward_chaining_GAE_application implements
		EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	private RootPanel rootPanel;

	private ChainingPanel chainingPanel;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		rootPanel = RootPanel.get("content");
		chainingPanel = new ChainingPanel();
		rootPanel.add(chainingPanel );
	}
}

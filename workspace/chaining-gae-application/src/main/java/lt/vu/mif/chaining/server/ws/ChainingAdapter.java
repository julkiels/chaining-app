package lt.vu.mif.chaining.server.ws;

import lt.vu.mif.chaining.ChainingService;
import lt.vu.mif.chaining.ChainingService.ChainingResult;

public class ChainingAdapter {
	
	private ChainingService chaining = new ChainingService();
	
	public lt.vu.mif.chaining.jaxws.ClientQueryResponse chaining(lt.vu.mif.chaining.jaxws.ClientQuery req){
		ChainingResult result = chaining.clientQuery(req.getMode(), req.getRules(), req.getAssertions(), req.getGoal());
		lt.vu.mif.chaining.jaxws.ClientQueryResponse response = new lt.vu.mif.chaining.jaxws.ClientQueryResponse();
		response.setReturn(result);
		return response;
	}

}
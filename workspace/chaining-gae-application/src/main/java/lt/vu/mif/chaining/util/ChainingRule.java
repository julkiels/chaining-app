package lt.vu.mif.chaining.util;

import java.util.ArrayList;
import java.util.List;

import kr.Rule;

public class ChainingRule extends Rule {


	public ChainingRule(ChainingSymbol consequent,
			List<ChainingSymbol> antecedents, int ruleNumber, boolean b) {
		super(consequent.toString(), toStringList(antecedents), ruleNumber, b   );
	}

	private static List<String> toStringList(List<ChainingSymbol> parsedSymbols) {
		ArrayList<String> returnList = new ArrayList<String>();
		for(ChainingSymbol cs: parsedSymbols){
			returnList.add(cs.toString());
		}
		return returnList;
	}

	
}

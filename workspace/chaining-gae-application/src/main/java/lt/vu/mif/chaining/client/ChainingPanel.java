package lt.vu.mif.chaining.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class ChainingPanel extends Composite implements HasText {

	private static final String FORWARD = "FORWARD";

	private static final String BACKWARD = "BACKWARD";

	private static ChainingPanelUiBinder uiBinder = GWT
			.create(ChainingPanelUiBinder.class);
	
	private ChainingServiceAsync service = GWT.create(ChainingService.class);

	interface ChainingPanelUiBinder extends UiBinder<Widget, ChainingPanel> {
	}

	public ChainingPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		chainingType.addItem(BACKWARD,BACKWARD);
		chainingType.addItem(FORWARD,FORWARD);
	}
	
	@UiField
	ListBox chainingType;

	@UiField
	Button button;
	
	@UiField
	Button convertButton;
	
	@UiField
	TextArea rulesArea;
	
	@UiField
	TextBox assertionsArea;
	
	@UiField
	TextBox goalArea;
	
	@UiField
	TextBox ruleSeqResult;
	
	@UiField
	Label ruleSeqResultLabel;
	
	@UiField
	InlineLabel goalReachedLabel;
	
	@UiField
	TextBox goalReachedInput;

	@UiField
	TextArea resultArea;
	
	@UiField
	TextArea conversionResultArea;

	private AsyncCallback<ChainingResp> buttonClickCallback;
	
	private AsyncCallback<String> conversionButtonClickCallback;
	
	@UiHandler("button")
	void onClick(ClickEvent e) {
		button.setEnabled(false);
		lazyInitButtonClickCallback();
		if (FORWARD.equalsIgnoreCase(chainingType.getValue(chainingType
				.getSelectedIndex()))) {
			service.forwardChaining(rulesArea.getText(),
					assertionsArea.getText(), goalArea.getText(), buttonClickCallback);
		} else if (BACKWARD.equalsIgnoreCase(chainingType.getValue(chainingType
				.getSelectedIndex()))) {
			service.backwardChaining(rulesArea.getText(),
					assertionsArea.getText(), goalArea.getText(), buttonClickCallback);
		}
	}
	
	@UiHandler("convertButton")
	void onConvertButtonClick(ClickEvent e){
		lazyInitConversionButtonClickCallback();
		service.toJson(chainingType.getValue(chainingType.getSelectedIndex()), rulesArea.getText(), assertionsArea.getText(), goalArea.getText(), conversionButtonClickCallback);
	}

	private void lazyInitButtonClickCallback() {
		hideResultFields();
		if (buttonClickCallback == null) {
			buttonClickCallback = new AsyncCallback<ChainingResp>() {

				@Override
				public void onSuccess(ChainingResp result) {
					resultArea.setValue(result.getExecution());
					resultArea.setVisible(true);
					goalReachedLabel.setVisible(true);
					goalReachedInput.setValue(result.getGoalReached() ? "True" : "False");
					ruleSeqResult.setText(result.getRuleseq());
					ruleSeqResultLabel.setVisible(true);
					goalReachedInput.setVisible(true);
					ruleSeqResult.setVisible(true);
					button.setEnabled(true);
				}

				@Override
				public void onFailure(Throwable caught) {
					resultArea.setValue(caught.toString());
					goalReachedLabel.setVisible(true);
					goalReachedInput.setVisible(true);
					goalReachedInput.setValue("False");
					resultArea.setVisible(true);
					button.setEnabled(true);
				}
			};
		}
	}

	private void hideResultFields() {
		conversionResultArea.setVisible(false);
		resultArea.setVisible(false);
		goalReachedLabel.setVisible(false);
		goalReachedInput.setVisible(false);
		ruleSeqResultLabel.setVisible(false);
		ruleSeqResult.setVisible(false);
		ruleSeqResult.setText("");
	}
	
	private void lazyInitConversionButtonClickCallback(){
		hideResultFields();
		if(conversionButtonClickCallback == null){
			conversionButtonClickCallback = new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					conversionResultArea.setValue(caught.toString());
					conversionResultArea.setVisible(true);
					convertButton.setEnabled(true);
				}

				@Override
				public void onSuccess(String result) {
					conversionResultArea.setValue(result);
					conversionResultArea.setVisible(true);
					convertButton.setEnabled(true);
				}
			};
		}
	}

	public void setText(String text) {
		button.setText(text);
	}

	public String getText() {
		return button.getText();
	}

}

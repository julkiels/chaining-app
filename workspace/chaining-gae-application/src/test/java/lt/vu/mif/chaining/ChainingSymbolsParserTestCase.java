package lt.vu.mif.chaining;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import lt.vu.mif.chaining.util.ChainingSymbol;
import lt.vu.mif.chaining.util.ChainingSymbolsParser;

import org.junit.Test;

public class ChainingSymbolsParserTestCase {

	@Test(expected=IllegalArgumentException.class)
	public void testIfEmpty() {
		ChainingSymbolsParser.parse("");
		ChainingSymbolsParser.parse(",/.;l,/.");
	}
	
	@Test
	public void testReturn(){
		List<ChainingSymbol> list = new ArrayList<ChainingSymbol>();
		list.add(new ChainingSymbol("a"));
		list.add(new ChainingSymbol("b"));
		list.add(new ChainingSymbol("c"));
		TestCase.assertTrue(list.equals(ChainingSymbolsParser.parse("ABC")));
		list.add(new ChainingSymbol("d"));
		TestCase.assertFalse(list.equals(ChainingSymbolsParser.parse("ABC")));
		TestCase.assertTrue(list.equals(ChainingSymbolsParser.parse("ABCD")));
		TestCase.assertFalse(list.equals(ChainingSymbolsParser.parse("ABCE")));
		
		list.add(new ChainingSymbol("a12"));
		TestCase.assertTrue(list.equals(ChainingSymbolsParser.parse("ABCDA12")));
		list.add(new ChainingSymbol("b126"));
		TestCase.assertTrue(list.equals(ChainingSymbolsParser.parse("ABCDA12b126")));
		TestCase.assertFalse(list.equals(ChainingSymbolsParser.parse("ABCDA12b12")));
		TestCase.assertTrue(list.equals(ChainingSymbolsParser.parse("ABCDA12b126// asfhjsdkfn askj")));
	}

}


package lt.vu.mif.chaining.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "clientQueryResponse", namespace = "http://chaining.mif.vu.lt/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clientQueryResponse", namespace = "http://chaining.mif.vu.lt/")
public class ClientQueryResponse {

    @XmlElement(name = "return", namespace = "")
    private lt.vu.mif.chaining.ChainingService.ChainingResult _return;

    /**
     * 
     * @return
     *     returns ChainingResult
     */
    public lt.vu.mif.chaining.ChainingService.ChainingResult getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(lt.vu.mif.chaining.ChainingService.ChainingResult _return) {
        this._return = _return;
    }

}

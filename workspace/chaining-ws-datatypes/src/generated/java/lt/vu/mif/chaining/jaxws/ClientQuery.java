
package lt.vu.mif.chaining.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "clientQuery", namespace = "http://chaining.mif.vu.lt/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clientQuery", namespace = "http://chaining.mif.vu.lt/", propOrder = {
    "mode",
    "rules",
    "assertions",
    "goal"
})
public class ClientQuery {

    @XmlElement(name = "mode", namespace = "")
    private lt.vu.mif.chaining.ChainingService.ChainingMode mode;
    @XmlElement(name = "rules", namespace = "")
    private lt.vu.mif.chaining.ChainingService.ChainingServiceRules rules;
    @XmlElement(name = "assertions", namespace = "")
    private lt.vu.mif.chaining.ChainingService.ChainingServiceAssertions assertions;
    @XmlElement(name = "goal", namespace = "")
    private String goal;

    /**
     * 
     * @return
     *     returns ChainingMode
     */
    public lt.vu.mif.chaining.ChainingService.ChainingMode getMode() {
        return this.mode;
    }

    /**
     * 
     * @param mode
     *     the value for the mode property
     */
    public void setMode(lt.vu.mif.chaining.ChainingService.ChainingMode mode) {
        this.mode = mode;
    }

    /**
     * 
     * @return
     *     returns ChainingServiceRules
     */
    public lt.vu.mif.chaining.ChainingService.ChainingServiceRules getRules() {
        return this.rules;
    }

    /**
     * 
     * @param rules
     *     the value for the rules property
     */
    public void setRules(lt.vu.mif.chaining.ChainingService.ChainingServiceRules rules) {
        this.rules = rules;
    }

    /**
     * 
     * @return
     *     returns ChainingServiceAssertions
     */
    public lt.vu.mif.chaining.ChainingService.ChainingServiceAssertions getAssertions() {
        return this.assertions;
    }

    /**
     * 
     * @param assertions
     *     the value for the assertions property
     */
    public void setAssertions(lt.vu.mif.chaining.ChainingService.ChainingServiceAssertions assertions) {
        this.assertions = assertions;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getGoal() {
        return this.goal;
    }

    /**
     * 
     * @param goal
     *     the value for the goal property
     */
    public void setGoal(String goal) {
        this.goal = goal;
    }

}

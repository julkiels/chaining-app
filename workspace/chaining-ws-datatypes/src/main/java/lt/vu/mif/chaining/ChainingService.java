package lt.vu.mif.chaining;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import kr.AbstractChaining;
import kr.BackwardChaining;
import kr.ChainingProcessResult;
import kr.ForwardChaining;
import kr.Rule;

@WebService()
public class ChainingService {

	public enum ChainingMode {
		FORWARD, BACKWARD
	}

	public static class ChainingResult{

		private String executionSteps;
		private Boolean goalReached;
		private ChainingResultRuleSequences ruleSequences;

		public ChainingResult() {

		}

		public ChainingResult(ChainingProcessResult processingResult) {
			executionSteps = processingResult.getExecutionSteps();
			goalReached = processingResult.getGoalReached();
			setRuleSequences(new ChainingResultRuleSequences());
			getRuleSequences().setRuleSequence(processingResult.getRuleSequence());
//			processingResult.getRuleSequenceString();
		}

		public String getExecutionSteps() {
			return executionSteps;
		}

		public void setExecutionSteps(String executionSteps) {
			this.executionSteps = executionSteps;
		}

		public Boolean getGoalReached() {
			return goalReached;
		}

		public void setGoalReached(Boolean goalReached) {
			this.goalReached = goalReached;
		}

		public ChainingResultRuleSequences getRuleSequences() {
			return ruleSequences;
		}

		public void setRuleSequences(ChainingResultRuleSequences ruleSequences) {
			this.ruleSequences = ruleSequences;
		}
	}
	
	public static class ChainingResultRuleSequences{
		private List<String> ruleSequence;

		public List<String> getRuleSequence() {
			return ruleSequence;
		}

		public void setRuleSequence(List<String> ruleSequence) {
			this.ruleSequence = ruleSequence;
		}
	}

	public static class ChainingServiceRules{
		private List<ChainingServiceRule> rule;

		public List<ChainingServiceRule> getRule() {
			return rule;
		}

		public void setRule(List<ChainingServiceRule> rule) {
			this.rule = rule;
		}
	}
	
	public static class ChainingServiceRule {
		private String consequent;
		private ChainingServiceAntecedents antecedents;

		public String getConsequent() {
			return consequent;
		}

		public void setConsequent(String consequent) {
			this.consequent = consequent;
		}

		public ChainingServiceAntecedents getAntecedents() {
			return antecedents;
		}

		public void setAntecedents(ChainingServiceAntecedents antecedents) {
			this.antecedents = antecedents;
		}
	}
	
	public static class ChainingServiceAntecedents{
		private List<String> antecedent;

		public List<String> getAntecedent() {
			return antecedent;
		}

		public void setAntecedent(List<String> antecedent) {
			this.antecedent = antecedent;
		}
	}
	
	public static class ChainingServiceAssertions{
		private List<String> assertion;

		public List<String> getAssertion() {
			return assertion;
		}

		public void setAssertion(List<String> assertion) {
			this.assertion = assertion;
		}
	}

	@WebMethod(action="clientQuery")
	public ChainingResult clientQuery(
			@WebParam(name = "mode") ChainingMode chainingMode,
			@WebParam(name = "rules") ChainingServiceRules rulesListParam,
			@WebParam(name = "assertions") ChainingServiceAssertions assertionListParam,
			@WebParam(name = "goal") String goalParam) {
		List<Rule> rulesList = new ArrayList<Rule>();
		int ruleNr = 1;
		for(ChainingServiceRule serviceRule: rulesListParam.getRule()){
			rulesList.add(new Rule(serviceRule.getConsequent(), serviceRule.getAntecedents().getAntecedent(), ruleNr++, false));
		}

		LinkedList<String> assertionsList = new LinkedList<String>();
		assertionsList.addAll(assertionListParam.getAssertion());

		StringBuilder sb = new StringBuilder();
		AbstractChaining chainingProcessor = null;
		if (ChainingMode.FORWARD.equals(chainingMode)) {
			chainingProcessor = new ForwardChaining(goalParam,
					rulesList, assertionsList, sb);
		} else if (ChainingMode.BACKWARD.equals(chainingMode)) {
			chainingProcessor = new BackwardChaining(goalParam.toString(),
					rulesList, assertionsList, sb);
		} else {
			throw new IllegalArgumentException("Klaidingas isvedimo tipas:"
					+ chainingMode.toString());
		}
		return new ChainingResult(chainingProcessor.execute());
	}
}

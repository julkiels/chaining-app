# README #

This is main GIT repo for demonstrational Forward and backward chaining web application. Application can be accessed [here](http://juliuschainingexample.appspot.com). It has additional web service interface, it's wsdl descriptor can be downloaded [here](http://juliuschainingexample.appspot.com/ChainingServiceService.wsdl).

Source also contains PHP client which communicates with chaining web service. Client source lies in _Forward and Backward chaining WS client_ directory. Appcalition can be found [here](http://juliuschainingwsclient.herokuapp.com).

### Chaining Application system requirements ###

* JDK 1.7
* Maven 3
* Internet connection

### Chaining ws client system requirements ###
* Apache Http server
* PHP 5 

### Local setup ###

* Clone git repo
* Run 'mvn clean install' from _ChainingAppParentPom_ directory
* Run 'mvn appengine:devserver' from _chaining-gae-application_ directory

### Appengine deployment ###
* Run 'mvn appengine:update' from _chaining-gae-application_ directory

### post scriptum ###
* If you make any changes in _'ChainingService.java'_ file in _'chaining-ws-datatypes'_ please regenerate web service sources with maven command:
_'mvn clean install -P generate-sources'_